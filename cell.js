class cell {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.parent = -1
        this.color = [255, 255, 255];
        this.init = false;
        this.visited = false;
        this.neighbours = []
        this.availableNeighbours = [] // T R B L Clockwise order
        this.t = true
        this.b = true
        this.r = true
        this.l = true
        this.s = false
        this.e = false
        this.distance // Distance from the end
        this.weight // Sum of weights from the start
        this.totalWeight //Sum of weights from the start + distance
    }
    display() {
        push()
        noStroke()
        fill(this.color)
        rect(this.x + 1 + w / 4, this.y + 1 + w / 4, (w - 1) / 2, (w - 1) / 2);
        pop()
        if (this.t) {
            line(this.x, this.y, this.x + w, this.y)
        }
        if (this.b) {
            line(this.x, this.y + w, this.x + w, this.y + w)
        }
        if (this.r) {
            line(this.x + w, this.y, this.x + w, this.y + w)
        }
        if (this.l) {
            line(this.x, this.y, this.x, this.y + w)
        }
    }
    start() {
        this.color = [48, 240, 70];
        this.s = true
    }
    end() {
        this.color = [224, 20, 13];
        this.e = true
    }
    visit() {
        this.color = [196, 169, 122];
    }
    failed() {
        this.color = [56, 223, 252];
    }
    up() {
        if ((cells.indexOf(this) - (width - 1) / w) >= 0) {
            return cells.indexOf(this) - (width - 1) / w;
        } else {
            return -1
        }
    }
    down() {
        if ((cells.indexOf(this) + (width - 1) / w) < cells.length) {
            return cells.indexOf(this) + (width - 1) / w
        } else {
            return -1
        }
    }
    right() {
        if (!((cells.indexOf(this) + 1) % ((width - 1) / w) == 0)) {
            return cells.indexOf(this) + 1;
        } else {
            return -1
        }

    }
    left() {
        if (!(cells.indexOf(this) % ((width - 1) / w) == 0)) {
            return cells.indexOf(this) - 1;
        } else {
            return -1
        }
    }
    remove(x) {
        if (x == this.up()) {
            this.t = false
            cells[this.up()].b = false
        }
        else if (x == this.down()) {
            this.b = false
            cells[this.down()].t = false
        }
        else if (x == this.right()) {
            this.r = false
            cells[this.right()].l = false
        }
        else if (x == this.left()) {
            this.l = false
            cells[this.left()].r = false
        } else {
            console.log("The cell you wish to remove is not a neighbourgh cell. You are trying to remove the cell: " + x + ". This this cell is no: " + cells.indexOf(this) + ". This cells neighbour cells are: " + this.neighbours)
        }
    }
    removeTop() {
        this.t = false
    }
    removeBottom() {
        this.b = false
    }
    removeRight() {
        this.r = false
    }
    removeLeft() {
        this.l = false
    }
    openWalls() {
        if (!this.t && !cells[this.up()].visited) {
            this.availableNeighbours.push(this.up())
            console.log(this.up() + "Top")
        }
        if (!this.r && !cells[this.right()].visited) {
            this.availableNeighbours.push(this.right())
            console.log(this.right() + "Right")
        }
        if (!this.b && !cells[this.down()].visited) {
            this.availableNeighbours.push(this.down())
            console.log(this.down() + "Down")
        }
        if (!this.l && !cells[this.left()].visited) {
            this.availableNeighbours.push(this.left())
            console.log(this.left() + "Left")
        }
        return this.availableNeighbours
    }
    distanceFromEnd(x) {
        this.distance = Math.abs((cells[x].x - this.x) / w) + Math.abs((cells[x].y - this.y) / w)
        return this.distance
    }
}
