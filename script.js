const w = 10
var cells = [] // Grid of cells
var wallStack = [] // Array of visited cells in wall creation
var queue = []
var start = 0
var end = 9999




function createCells() {
	for (let y = 0; y < width - 1; y += w) {
		for (let x = 0; x < height - 1; x += w) {
			cells.push(new cell(x, y))
		}
	}
	for (c of cells) { // Create an array of neighbour cells for each cell
		if (c.up() != -1) {
			c.neighbours.push(c.up())
		}
		if (c.down() != -1) {
			c.neighbours.push(c.down())
		}
		if (c.right() != -1) {
			c.neighbours.push(c.right())
		}
		if (c.left() != -1) {
			c.neighbours.push(c.left())
		}
	}
}


function randomNeighbour(current) {
	return cells[current].neighbours[Math.floor(Math.random() * cells[current].neighbours.length)]
}

function wilson() {
	let completing = true
	let current = 0
	let next
	let initBranch = true
	let branch = true
	let temp
	let goBack
	let available = []

	while (initBranch) { // Initialising Wilson's algorithm 
		wallStack.push(current)
		cells[current].init = true
		next = randomNeighbour(current)
		while (next == goBack) {
			next = randomNeighbour(current)
		}
		//console.log("Current: " + current + " Next: " + next + " goBack: " + goBack)
		if (cells[next].init == true) {
			//wallStack.push(next)
			initBranch = false
			while (wallStack.length != 0) {
				if (wallStack.length == 1) {
					wallStack.pop()
				} else {
					temp = wallStack.pop()
					cells[wallStack[wallStack.length - 1]].remove(temp)
				}
			}
		}
		goBack = current
		current = next
	}

	while (completing) {
		available = []
		for (c of cells) {	// Checks if there is any unvisited cells that havent created walls
			if (c.init == false) {
				available.push(cells.indexOf(c))
				completing = true
			}
		}
		//console.log(available)
		if (available.length != 0) {
			branch = true
			current = available[Math.floor(Math.random() * available.length)]
			//current = available[0]
			//cells[current].init = true
			goBack = -1
			while (branch) {
				//console.log(available)

				wallStack.push(current)
				//console.log(wallStack)
				next = randomNeighbour(current)
				while (next == goBack) {
					next = randomNeighbour(current)

				}
				if (cells[next].init == true) {
					//console.log(cells[next].init)
					//console.log(next)
					branch = false
					wallStack.push(next)
					while (wallStack.length != 0) {
						if (wallStack.length == 1) {
							temp = wallStack.pop()
							cells[temp].init = true
						} else {
							//console.log(wallStack)
							temp = wallStack.pop()
							cells[temp].init = true
							//console.log(wallStack)
							//console.log(temp, wallStack[wallStack.length - 1])
							cells[wallStack[wallStack.length - 1]].remove(temp)
						}
					}
				} else if (wallStack.includes(next)) {
					wallStack.splice(wallStack.indexOf(next))
					if (wallStack[wallStack.length - 2] == undefined) {
						goBack = -1
					} else {
						goBack = wallStack[wallStack.length - 2]
					}
					//console.log(next, wallStack[wallStack.length - 1])
					current = next
				} else {
					//console.log(goBack, current, next)
					goBack = current
					current = next

				}
			}
		} else {
			completing = false
		}
	}
}

function aStar() {
	let temp
	let run = true
	queue.push(start)
	cells[start].start()
	cells[end].end()
	cells[start].weight = 0
	cells[start].visited = true
	while (run) {
		if (queue[0] == end) {
			console.log("IS SHOULDNT BE HERE")
			let backtrack = true
			let current = queue[0]
			while (backtrack) {
				if (current != start) {
					cells[current].visit()
					current = cells[current].parent
				} else {
					backtrack = false
				}
			}
			//Implement the backtracking and colouring
			run = false
		} else {
			temp = queue.shift()
			cells[temp].openWalls()
			console.log(temp)
			for (i = 0; i < cells[temp].availableNeighbours.length; i++) {
				queue.push(cells[temp].availableNeighbours[i])
				console.log(cells[temp].availableNeighbours[i])
				cells[cells[temp].availableNeighbours[i]].weight = cells[temp].weight + 1 // The 1 is the weight to travel from one cell to another
				cells[cells[temp].availableNeighbours[i]].totalWeight = cells[cells[temp].availableNeighbours[i]].weight + cells[cells[temp].availableNeighbours[i]].distanceFromEnd(end)
				cells[cells[temp].availableNeighbours[i]].parent = temp
				cells[cells[temp].availableNeighbours[i]].visited = true
				//cells[cells[temp].availableNeighbours[i]].failed() // Displays the cells that have been visited unsuccessfully
			}
			temp = []
			console.log(queue)
			if (queue.length > 1) {
				bubbleSort()
			}
			console.log(queue)
		}
	}
	cells[end].end()
}

function bubbleSort() {
	var count = 0;
	do {
		var swap = false;
		for (var i = 1; i < queue.length - count; i++) {
			if (cells[queue[i]].totalWeight < cells[queue[i - 1]].totalWeight) {
				temp = queue[i];
				queue[i] = queue[i - 1];
				queue[i - 1] = temp;
			}
			swap = true;
		}
		count++;
	} while (swap);
}


function compareNumbers(a, b) {
	console.log("comparing: " + cells[queue[a]].totalWeight + " with: " + cells[queue[b]].totalWeight)
	return cells[queue[a]].totalWeight - cells[queue[b]].totalWeight
}

function drawCells() {
	for (c of cells) {
		c.display()
	}
}


function setup() {
	createCanvas(1001, 1001)
	createCells()
	wilson()
	aStar()
}


function draw() {
	//background(230)
	drawCells()

}

